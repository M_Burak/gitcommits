package com.burak.kotlinrepo

import androidx.multidex.MultiDexApplication
import com.burak.kotlinrepo.di.component.AppComponent
import com.burak.kotlinrepo.di.component.DaggerAppComponent
import com.burak.kotlinrepo.di.module.GitRepoUsecaseModule
import com.burak.kotlinrepo.di.module.NetworkModule
import com.burak.kotlinrepo.di.module.RepositoryModule


@Suppress("DEPRECATION")
class BaseApp : MultiDexApplication() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        this.appComponent = this.initDagger()
    }

    private fun initDagger() = DaggerAppComponent.builder()
        .networkModule(NetworkModule())
        .gitRepoUsecaseModule(GitRepoUsecaseModule())
        .repositoryModule(RepositoryModule())
        .build()


}