package com.burak.kotlinrepo.di.module

import androidx.lifecycle.ViewModel
import com.burak.kotlinrepo.viewmodel.ListViewModel
import com.burak.kotlinrepo.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ListViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    internal abstract fun bindMyViewModel(listViewModel: ListViewModel): ViewModel
}