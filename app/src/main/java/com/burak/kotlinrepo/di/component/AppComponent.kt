package com.burak.kotlinrepo.di.component

import com.burak.kotlinrepo.di.module.GitRepoUsecaseModule
import com.burak.kotlinrepo.di.module.NetworkModule
import com.burak.kotlinrepo.di.module.RepositoryModule
import com.burak.kotlinrepo.di.scope.AppScope
import com.burak.kotlinrepo.di.subcomponent.ViewModelComponent
import dagger.Component

@AppScope
@Component(
    modules = [
        NetworkModule::class,
        RepositoryModule::class,
        GitRepoUsecaseModule::class
    ]
)
interface AppComponent {
    fun newCommitListComponent(): ViewModelComponent
}