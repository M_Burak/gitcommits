package com.burak.kotlinrepo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.burak.kotlinrepo.di.component.DaggerAppComponent
import com.burak.kotlinrepo.di.module.GitRepoUsecaseModule
import com.burak.kotlinrepo.di.module.NetworkModule
import com.burak.kotlinrepo.di.module.RepositoryModule
import com.burak.kotlinrepo.domain.GitRepoUseCase
import com.burak.kotlinrepo.model.*
import com.burak.kotlinrepo.viewmodel.ListViewModel
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

class ListViewModelTest {

    @get:Rule
    var rule = InstantTaskExecutorRule()


    val api = Mockito.mock(GitRepoApi::class.java)
    val service = GitRepoService(api)
    val useCase = GitRepoUseCase(service)


    @InjectMocks
    var listViewModel = ListViewModel(useCase)

    private var response: List<CommitResponse>? = null
    private var commitResponseObservable : Observable<List<CommitResponse>>? = null


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        DaggerAppComponent.builder()
            .networkModule(NetworkModule())
            .gitRepoUsecaseModule(GitRepoUsecaseModule())
            .repositoryModule(RepositoryModule())
            .build()
    }

    @Before
    fun setUpRxSchedulers() {
        val immediate = object : Scheduler() {

            override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
                return super.scheduleDirect(run, 0, unit)
            }

            override fun createWorker(): Scheduler.Worker {
                return ExecutorScheduler.ExecutorWorker(Executor { it.run() }, true)
            }
        }

        RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
        RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
    }

    @Test
    fun getCommitsSuccess() {


        loadCommitResponse()
        commitResponseObservable = Observable.just(response)

        Mockito.`when`(api.getCommits())
            .thenReturn(commitResponseObservable)
        listViewModel.getCommits()

        Assert.assertEquals(1, listViewModel.commits.value?.size)
        Assert.assertEquals(false, listViewModel.loadError.value)
        Assert.assertEquals(false, listViewModel.loading.value)

    }

    @Test
    fun getCommitsFail() {
        loadCommitResponse()

        commitResponseObservable = Observable.error(Throwable())

        Mockito.`when`(api.getCommits()).thenReturn(commitResponseObservable)
        listViewModel.getCommits()
        Assert.assertEquals(true, listViewModel.loadError.value)
        Assert.assertEquals(false, listViewModel.loading.value)
    }

    fun loadCommitResponse(){
       val author = Author("...","","","","","","",1,
           "","","","","",false,"","","","")
        val responseX = CommitResponse(author,"",null,null,"","",null,"","")
        response= listOf(responseX)

    }

}