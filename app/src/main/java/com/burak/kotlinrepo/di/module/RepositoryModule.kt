package com.burak.kotlinrepo.di.module

import com.burak.kotlinrepo.di.scope.AppScope
import com.burak.kotlinrepo.model.GitRepoApi
import com.burak.kotlinrepo.model.GitRepoService
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule{
    @AppScope
    @Provides
    fun provideFeedRepository(api: GitRepoApi) = GitRepoService(api)
}