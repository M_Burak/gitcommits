package com.burak.kotlinrepo.viewmodel

import androidx.lifecycle.MutableLiveData
import com.burak.kotlinrepo.domain.GitRepoUseCase
import com.burak.kotlinrepo.model.CommitResponse
import com.burak.kotlinrepo.model.Response
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class ListViewModel @Inject constructor(private val useCase: GitRepoUseCase) : BaseViewModel() {

    val commits = MutableLiveData<List<CommitResponse>>()
    val loadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    fun getCommits() {
        loading.value = true
        useCase.getCommits()?.subscribeWith(object : DisposableObserver<List<CommitResponse>>() {
            override fun onComplete() {

            }

            override fun onNext(t: List<CommitResponse>) {
                commits.value = t
                loadError.value = false
                loading.value = false
            }

            override fun onError(e: Throwable) {
                loadError.value = true
                loading.value = false
            }


        })?.let {
            disposable.add(
                it
            )
        }
    }
}