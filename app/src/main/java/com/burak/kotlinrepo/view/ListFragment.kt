package com.burak.kotlinrepo.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.burak.kotlinrepo.BaseApp

import com.burak.kotlinrepo.R
import com.burak.kotlinrepo.model.CommitResponse
import com.burak.kotlinrepo.viewmodel.DaggerViewModelFactory
import com.burak.kotlinrepo.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class ListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: DaggerViewModelFactory

    private lateinit var listViewModel: ListViewModel
    private val listAdapter = CommitListAdapter(arrayListOf())
    private val commitListDataObserver = Observer<List<CommitResponse>> { list ->
        list?.let {
            listAdapter.updateCommitList(list)
        }
    }

    private val loadingLiveDataObserver = Observer<Boolean> { isLoading ->

        pBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        if (isLoading) {
            tvError.visibility = View.GONE
            rwCommits.visibility = View.GONE
        }else
            rwCommits.visibility = View.VISIBLE

    }

    private val loadErrorLiveDataObserver = Observer<Boolean> { loadingError ->
        tvError.visibility = if (loadingError) View.VISIBLE else View.GONE
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).supportActionBar?.title = getString(R.string.appTitle)
        (activity?.applicationContext as BaseApp).appComponent
            .newCommitListComponent().inject(this)
        listViewModel = ViewModelProviders.of(this, viewModelFactory)[ListViewModel::class.java]

        rwCommits.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }

        observeCommitList()
        listViewModel.getCommits()
        observeCommitList()
    }


    private  fun observeCommitList() {
        listViewModel.commits.observe(this, commitListDataObserver)
        listViewModel.loading.observe(this, loadingLiveDataObserver)
        listViewModel.loadError.observe(this, loadErrorLiveDataObserver)
    }

    override fun getLayoutById() = R.layout.fragment_list

}
