package com.burak.kotlinrepo.domain

import com.burak.kotlinrepo.model.CommitResponse
import com.burak.kotlinrepo.model.GitRepoService
import com.burak.kotlinrepo.model.Response
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GitRepoUseCase(private val service:GitRepoService) {

    fun getCommits(): Observable<List<CommitResponse>>? = service.getCommits()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}