package com.burak.kotlinrepo.di.module
import com.burak.kotlinrepo.di.scope.AppScope
import com.burak.kotlinrepo.domain.GitRepoUseCase
import com.burak.kotlinrepo.model.GitRepoService
import dagger.Module
import dagger.Provides

@Module
class GitRepoUsecaseModule {
    @AppScope
    @Provides
    fun provideFeedUseCase(service: GitRepoService) = GitRepoUseCase(service)
}