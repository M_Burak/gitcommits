package com.burak.kotlinrepo.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.burak.kotlinrepo.model.CommitResponse
import com.burak.kotlinrepo.util.getProgressDrawable
import com.burak.kotlinrepo.util.loadImage
import kotlinx.android.synthetic.main.item_commit.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import com.burak.kotlinrepo.R


class CommitListAdapter(var commits: ArrayList<CommitResponse>) :
    RecyclerView.Adapter<CommitListAdapter.CommitViewHolder>() {
    fun updateCommitList(newCommits: List<CommitResponse>?) {
        commits.clear()
        if (newCommits != null) {
            commits.addAll(newCommits)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CommitViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_commit, parent, false)


    )

    override fun getItemCount() = commits.size

    override fun onBindViewHolder(holder: CommitViewHolder, position: Int) {
        commits[position].let { holder.bind(it) }
        holder.itemView.clCommits.setOnClickListener {
        }
    }

    class CommitViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val committer = view.tvCommitter
        private val commitTitle = view.tvCommitTitle
        private val commitDate = view.tvCommitMessage
        private val commiterPhoto = view.ivCommitter

        private val progressDrawable = getProgressDrawable(view.context)

        fun bind(commitResponse: CommitResponse) {
            committer.text = commitResponse.commit?.author?.name
            commitTitle.text = commitResponse.commit?.message
            commitDate.text = formatDate(commitResponse.commit?.committer?.date)
            commiterPhoto.loadImage(commitResponse.author?.avatar_url,progressDrawable)
        }

        private fun formatDate(date: String?): String? {
            val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.UK)
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val date = formatter.parse(date)
            return date.toString()
        }
    }

}