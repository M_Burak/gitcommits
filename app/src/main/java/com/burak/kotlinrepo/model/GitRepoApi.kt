package com.burak.kotlinrepo.model

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface GitRepoApi {

    @GET("jetbrains/kotlin/commits")
    fun getCommits(): Observable<List<CommitResponse>>
}