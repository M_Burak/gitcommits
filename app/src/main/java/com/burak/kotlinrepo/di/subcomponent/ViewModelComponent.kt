package com.burak.kotlinrepo.di.subcomponent

import com.burak.kotlinrepo.di.module.ListViewModelModule
import com.burak.kotlinrepo.di.module.ViewModelFactoryModule
import com.burak.kotlinrepo.di.scope.FragmentScope
import com.burak.kotlinrepo.view.ListFragment
import dagger.Subcomponent

@FragmentScope
@Subcomponent(
    modules = [
        ViewModelFactoryModule::class,
        ListViewModelModule::class]
)
interface ViewModelComponent{

    fun inject(listFragment: ListFragment)

}