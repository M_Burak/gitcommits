package com.burak.kotlinrepo.model

import io.reactivex.Observable

class GitRepoService(val api:GitRepoApi ) {

    fun getCommits(): Observable<List<CommitResponse>> {
        return api.getCommits()
    }
}